#!/usr/bin/env python
import os
import shutil
from pathlib import Path


HOME = str(Path.home())
PATHS = [os.path.join(HOME, ".mono"), os.path.join(HOME, ".designer")]
CONTENT_PATHS = [os.path.join(HOME, ".cache"), os.path.join(HOME, ".thumbnails")]


def main():
    clean(PATHS, CONTENT_PATHS)


def clear_content(path):
    (dirpath, dirnames, filenames) = next(os.walk(path))
    [os.remove(os.path.join(dirpath, file)) for file in filenames]
    [shutil.rmtree(os.path.join(dirpath, directory)) for directory in dirnames]


def remove_directory(path):
    shutil.rmtree(path)


def remove_file(path):
    os.remove(path)


def clean_path(path, content=False):
    if not os.path.exists(path):
        return

    if content and os.path.isdir(path):
        clear_content(path)
    else:
        if os.path.isdir(path):
            remove_directory(path)
        else:
            remove_file(path)


def clean(paths=None, content_paths=None):
    if paths is not None:
        [clean_path(path) for path in paths]
    if content_paths is not None:
        [clean_path(path, content=True) for path in content_paths]


main()
